function overlayVisible() {
    var id = document.getElementById("popup1");
    if (id.style.visibility === "hidden") {
    id.style.visibility = "visible";
    id.style.opacity = "1";
    } 
    else {
    id.style.visibility = "hidden";
    id.style.opacity = "0";
    }
}

Chart.defaults.color = "#e3e3e3"
Chart.defaults.borderColor = "#919191"

const barCanvas1 = document.getElementById("barCanvas1");

const barChart1 = new Chart(barCanvas1, {
    type: "bar",
    data: {
        labels: [
            "a profané des injures", 
            "s'est autodétruit", 
            "a parlé dans un langage indéchiffrable", 
            "a prit la fuite", 
            "essai réussi"
        ],
        datasets: [{
            label : 'Résultats des essais avec la première simulation',
            data: [9550,1941,19324,1240,62931],
            backgroundColor: [
                "#fc0335",
                "#8403fc",
                "#03fc80",
                "#fc8403",
                "#6bfc03"
                ]
        }]
    },
    options: {
        scales : {
            y: {
                suggestedMax: 100000,
            }
        },
        plugins:{
            
        }
    }
})


const barCanvas2 = document.getElementById("barCanvas2");

const barChart2 = new Chart(barCanvas2, {
    type: "bar",
    data: {
        labels: [
            "a profané des injures", 
            "s'est autodétruit", 
            "a parlé dans un langage indéchiffrable", 
            "a prit la fuite", 
            "essai réussi"
        ],
        datasets: [{
            label : 'Résultats des essais avec la dernière simulation',
            data: [1007,764,8864,2477,92299],
            backgroundColor: [
                "#fc0335",
                "#8403fc",
                "#03fc80",
                "#fc8403",
                "#6bfc03"
                ]
        }]
    },
    options: {
        scales : {
            y: {
                suggestedMax: 100000,
            }
        },
    }
})